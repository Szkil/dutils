package dutils

import (
	"fmt"
	"testing"
	"time"
)

/*
   - I need to make a decision on which way I am going to implement Money data type. (decimal)
*/

func Test_No(t *testing.T) {
	// todo: This is not doing anything - correct this
	ldtDay := time.Date(2001, 1, 16, 0, 0, 0, 0, time.UTC)

	if ldtDay.Day() != 16 {
		t.Error("Day did not return expected value of 16 :- ", ldtDay.Day())
	}
}

func Test_Comparef32(t *testing.T) {

	if Comparef32(1.54, 1.54) == false {
		t.Error("Comparison Failure of 1.54 and 1.54 ")
	}
	if Comparef32(1.55, 1.54) == true {
		t.Error("Comparison Failure of 1.54 and 1.55 ")
	}
	if Comparef32(1.5432, 1.5432) == false {
		t.Error("Comparison Failure of 1.54 and 1.54 ")
	}
	if Comparef32(1.5501, 1.54) == true {
		t.Error("Comparison Failure of 1.5501 and 1.54 ")
	}
	if Comparef32(1, 1.54) == true {
		t.Error("Comparison Failure of 1 and 1.54 ")
	}

}

func TestRoundaRoundtocents(t *testing.T) {

	lnAmount := float32(0)

	lnResult := Roundtocents(lnAmount, 100, 0 )
	if Comparef32(lnAmount, lnResult) == false {
		t.Error("Rounding failed 0 = 0 ")
	}

	// Note this rounding to 100 cents so whole dollars
	lnAmount = 0
	lnResult = Roundtocents(lnAmount, 100, 0 )
	if Comparef32(0, lnResult) == false {
		t.Error("Rounding failed 0.01 = 0.01 ")
	}

	lnAmount = 0.0
	lnResult = Roundtocents(lnAmount, 1, 0 )
	if Comparef32(lnAmount, lnResult) == false {
		t.Error("Rounding failed 0 = 0 ")
	}

	// todo this is failing - 0.01 is outputting 0.02
	lnAmount = 0.01
	lnResult = Roundtocents(lnAmount, 1, 0 )
	if Comparef32(lnAmount, lnResult) == false {
		t.Error("Rounding failed 0.01 = 0.01 ")
	}

	lnAmount = 1503.66
	lnResult = Roundtocents(lnAmount, 100, 0 )
	if Comparef32(lnResult, 1504) == false {
		t.Error(fmt.Sprintf("Rounding failed 1504 = %v ", lnResult))
	}


}

func TestRoundaFloat(t *testing.T) {
	lnAmount := float32(0)

   lnResult := RoundaFloat32(lnAmount, 2)
	if Comparef32(lnAmount, lnResult) == false {
		t.Error("Rounding failed 0 = 0 ")
	}

	lnAmount = 0.01
	lnResult = RoundaFloat32(lnAmount, 2 )
	if Comparef32(lnAmount, lnResult) == false {
		t.Error("Rounding failed 0.01 = 0.01 ")
	}

	// lnTotal := PD.Hrsunits()*PD.Rate()*PD.Pay_items().Factor()
   // lnAmount =



}