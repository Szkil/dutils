package dutils

import (
	"github.com/chewxy/math32"
	"math"
	"strconv"
)


/*
   - I need to make a decision on which way I am going to implement Money data type. (decimal)
*/
func DivZer32(x float32, y float32) float32 {
//	x / y
	if (x == 0) || (y == 0) {
		return 0
	} else {
		return x / y
	}
}

func DivZer64(x float64, y float64) float64 {
//	x / y
	if (x == 0) || (y == 0) {
		return 0
	} else {
		return x / y
	}
}


// I need to make a decision on what rounding method I will follow.
// https://groups.google.com/forum/#!topic/golang-nuts/ITZV08gAugI
// needs to some further understand and be clear on what is going on.

//func RoundFloat(rValue: extended; iPlaces: integer): extended;
//func RoundCurrency(rValue: currency; iPlaces: integer): currency;
//func Roundtocents(rValue: currency; iCents : integer; aiRoundsUpDn : integer = 0) : currency;
// I am likely to need to work what storage the currency data will be stored at.

// custom Roundtocents - that can handle negative values.
// checked against previous the previous numutils version only handled positive numbers
// todo valid cents : 100 to round to dollars  1 - to 0.01c
func Roundtocents(rValue float32, iCents int, aiRoundsUpDn int) float32 {
	liResult := rValue

	if iCents == 0 {
		return liResult
	}
	if rValue == 0 {
		return liResult
	}

	rCents := float32(iCents)
	rrValue := rValue

	liValue := float32(int(rrValue * 100)) // (rValue * 100)
	liFrac := (rrValue * 100) - liValue
	if liFrac >= 0.5 {
		liValue = liValue + 1
	}

	liMod := math32.Mod(liValue, rCents)
	if liMod > 0 {
		switch aiRoundsUpDn {
		case 0:
			{
				lnHalf := DivZer32(rCents, 2)
				if liMod >= lnHalf {
					// Round up
					liValue = liValue + (rCents - liMod)
				} else {
					// Round dn
					liValue = liValue - liMod
				}
			}
		case 1:
			{
				liValue = liValue + (rCents - liMod)
			}
		case -1:
			{
				liValue = liValue - liMod
			}

		}
	} else if liMod < 0 {
		liMod = math32.Abs(liMod)

		switch aiRoundsUpDn {
		case 0:
			{
				lnHalf := DivZer32(rCents, 2)
				if liMod >= lnHalf {
					liValue = liValue - (rCents - liMod)
				} else {
					liValue = liValue + liMod
				}
			}
		case 1:
			{
				liValue = liValue - (rCents - liMod)
			}
		case -1:
			{
				liValue = liValue + liMod
			}
		}

	}

	return float32(DivZer32(liValue, 100))
}

func Roundx(x, unit float64) float64 {
	return float64(int64(x/unit+0.5)) * unit
}

// http://stackoverflow.com/questions/19238175/what-is-the-best-way-to-convert-a-currency-from-float-to-integer-in-go
// worth a read
// return rounded version of x with prec precision.
func RoundFloat(x float64, prec int) float64 {
	frep := strconv.FormatFloat(x, 'g', prec, 64)
	f, _ := strconv.ParseFloat(frep, 64)
	return f
}

func Round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}

func RoundaFloat(x float64, prec int) float64 {

	if x == 0 {
		return x
	}

	var rounder float64
	pow := math.Pow(10, float64(prec))
	intermed := x * pow
	_, frac := math.Modf(intermed)
	intermed += .5
	x = .5
	if frac < 0.0 {
		x = -.5
		intermed -= 1
	}
	if frac >= x {
		rounder = math.Ceil(intermed)
	} else {
		rounder = math.Floor(intermed)
	}

	return rounder / pow
}

// Process as a 64bit then bring it back.
func RoundaFloat32(x float32, prec int) float32 {

	if x == 0 {
		return x
	}

	var rounder float32
	x32 := float32(x)
	pow := math32.Pow(10, float32(prec))
	intermed := x32 * pow
	_, frac := math32.Modf(intermed)
	intermed += .5
	x = .5
	if frac < 0.0 {
		x = -.5
		intermed -= 1
	}
	if frac >= x32 {
		rounder = math32.Ceil(intermed)
	} else {
		rounder = math32.Floor(intermed)
	}

	return rounder / pow
}

func IsNumber(n interface{}) (float64, bool) {
	switch n := n.(type) {
	case int:
		return float64(n), true
	case int8:
		return float64(n), true
	case int16:
		return float64(n), true
	case int32:
		return float64(n), true
	case int64:
		return float64(n), true
	case uint:
		return float64(n), true
	case uint8:
		return float64(n), true
	case uint16:
		return float64(n), true
	case uint32:
		return float64(n), true
	case uint64:
		return float64(n), true
	case float32:
		return float64(n), true
	case float64:
		return n, true
	default:
		return 0, false
	}
}

// Strtoi64 - string convert to int64
func Strtoi64(s string) (i int64, err error) {
	i64, err := strconv.ParseInt(s, 10, 0)
	return i64, err
}

func Comparef32(a, b float32) bool {
	const TOLERANCE = 0.0001

	diff := math32.Abs(b - a)
	return diff < TOLERANCE
}

func Comparef32to(a, b float32) bool {
	const TOLERANCE = 0.01

	diff := math32.Abs(b - a)
	return diff < TOLERANCE
}


/*
  TNumUtils = class
  public
    //class function  ExpXY(x, y: extended): extended;
    class function  DivZer(x, y: extended): extended;

    //class function  IntToFloat(x: longint): extended;
    class function  RoundFloat(rValue: extended; iPlaces: integer): extended;
    class function  RoundCurrency(rValue: currency; iPlaces: integer): currency;
    class function  Roundtocents(rValue: currency; iCents : integer; aiRoundsUpDn : integer = 0) : currency;

    class function  IsFloatEqual(rLeft,rRight: extended; iPlaces: integer) : boolean;
	// this is used in one place - but if there is a compare we don't need it.

    class function  Base36Char(isValue: shortint): char;

    class function  IsNumeric(const s: string): boolean;

    class function  GetACurrency(sStr: string; var rReal: currency): boolean;
    class function  GetAReal(sStr: string; var rReal: double): boolean;
    class function  GetAPercentage(sStr: string; var rReal: double): boolean;
    class function  GetAInteger(sStr: string; var iInt : integer) : boolean;

    { Bit manipulation }
    class function  IsBitSet(const val: longint; const TheBit: byte): boolean;
    class function  BitOn(const val: longint; const TheBit: byte): longint;
    class function  BitOff(const val: longint; const TheBit: byte): longint;
    class function  BitToggle(const val: longint; const TheBit: byte): longint;

    class procedure SetBit(var BitTable : Byte; BitMask : Byte);
    class procedure ClearBit(var BitTable : Byte; BitMask : Byte);
    class function  BitSet(BitTable, BitMask : Byte) : Boolean;

    class function  Ratio(Count : integer; Index : integer) : integer;

    class function  IsInteger (s : string) : boolean;
    class function  IsFloat (s : string) : boolean;

    class function  BinaryStrtoLongInt( Value : string ) : longint;

    class function  sDigit(Value : integer) : string;
    class function  DigitString(Value, Place : integer) : string;

    class function  CheckDigitMod10(Code : String) : String;
*/
