package dutils

import "time"
import "fmt"
import "strings"

/*
   todo: Turn the utils into a set Utils package.

*/

func Trunc(value float32) float32  {
   lnValue := int(value)
   return float32(lnValue)
}

// Month as int
func MonthasInt(value time.Time) int {
	m := value.Month()
	i := int(m)
	return i
}

func IsLeapYear(year int) bool {

	if year == 2000 {
		return true
	} else {
		if year%4 == 0 {
			if (year%100) != 0 || (year%400) == 0 {
				return true
			}
		}
		return false
	}
}

func GetYearDays(value time.Time) int {
	if IsLeapYear(value.Year()) {
		return 366
	} else {
		return 365
	}
}

func DaysInMonth(m time.Month, year int) int {
	return time.Date(year, m+1, 0, 0, 0, 0, 0, time.UTC).Day()
}

// this needs looking into as I dont believe its exactly what I want.
// consider Dates need time to be zero'd out - otherwise this can work ok.
// Always returns positive value.
func DaysBetween(dFromDate time.Time, dToDate time.Time) int {
	delta := dFromDate.Sub(dToDate)
	if delta < 0 {
		delta = delta * -1
	}

	return int(delta.Hours() / 24)
}

// There are a number of ways I can calculate this that depends on what I want to
// use it for
// decimalYears / whole years

func YearsBetween(dFromDate time.Time, dToDate time.Time) int {
	result := 0

	for i := 0; ; i++ {
		if dFromDate.AddDate(0, 1, 0).Before(dToDate) {
			dFromDate = dFromDate.AddDate(0, 1, 0)
			result++
		} else {
			break
		}
	}
	return result
}

func DecimalYearsBetween(dFromDate time.Time, dToDate time.Time) float64 {
	// lets see if I can work with a Day no.
	var dTo float64
	var dFrom float64

	dTo = Julian(dToDate)
	dFrom = Julian(dFromDate)

	return (dTo - dFrom) / 365 // 365.2545
}

func WeeksBetween(dFromDate time.Time, dToDate time.Time) float64 {
	var result float64
	var dtTo float64
	var dtFrom float64

	if dToDate.After(dFromDate) {
		dtFrom = Julian(dToDate)
		dtTo = Julian(dFromDate)
	} else {
		dtTo = Julian(dToDate)
		dtFrom = Julian(dFromDate)
	}

	if dtTo == dtFrom {
		result = 0
	} else {
		result = (dtFrom - dtTo) / 7
	}
	return result
}

// Assumes 1,1,Year is week 1
// Should use Date_dim as alternative
func GetWeekCalenderWeekNo(dDate time.Time) int {
	dStart := time.Date(dDate.Year(), 1, 1, 0, 0, 0, 0, time.UTC)
	return int(WeeksBetween(dStart, dDate))
}

// return just the Date
func TheDate(dDate time.Time) time.Time {
	liYear, liMonth, liDay := dDate.Date()
	return time.Date(liYear, liMonth, liDay, 0, 0, 0, 0, time.UTC)
}

// Return just the Time
// todo: Review when using this func on its behaviour.
func TheTime(dDate time.Time) time.Time {
	// time.clock - returns the time within the day
	// do I want a null date with a time? unsure just yet.

	liHour, liMin, liSec := dDate.Clock()
	return time.Date(0, 0, 0, liHour, liMin, liSec, 0, time.UTC)
}

// Normalized Age
func Age(dDOB time.Time, dDateTo time.Time) int {
	// Just want the Years
	lnAge := dDateTo.Year() - dDOB.Year()
	if dDateTo.Month() < dDOB.Month() {
		lnAge = lnAge - 1
	} else if (dDateTo.Month() == dDOB.Month()) && (dDateTo.Day() < dDOB.Day()) {
		lnAge = lnAge - 1
	}

	return lnAge
}

func AgenMonths(dDOB time.Time, dDateTo time.Time) (int, int) {
	// Just want the Years
	var lnMonthTo int
	var lnMonth int

	lnMonths := 0

	lnAge := dDateTo.Year() - dDOB.Year()
	lnMonth = int(dDOB.Month())
	lnMonthTo = int(dDateTo.Month())

	if lnMonthTo < lnMonth {
		lnAge = lnAge - 1
		for i := 1; ; i++ {
			lnMonth = lnMonth + 1
			if lnMonth > 12 {
				lnMonth = 1
			}
			if lnMonth == lnMonthTo {
				// check day as well - if on the it counts as a month otherwise not quite there yet.
				if dDateTo.Day() < dDOB.Day() {
					lnMonths = i - 1
				} else {
					lnMonths = i
				}
				break
			}
			if i > 12 {
				// error
				fmt.Println("Error in looping in AgenMonths")
				break
			}
		}
	} else if lnMonth < lnMonthTo {
		for i := 1; ; i++ {
			lnMonth = lnMonth + 1
			if lnMonth > 12 {
				lnMonth = 1
			}
			if lnMonth == lnMonthTo {
				// check day as well - if on the it counts as a month otherwise not quite there yet.
				if dDateTo.Day() < dDOB.Day() {
					lnMonths = i - 1
				} else {
					lnMonths = i
				}
				break
			}
			if i > 12 {
				// error
				fmt.Println("Error in looping in AgenMonths")
				break
			}
		}
	} else if (lnMonth == lnMonthTo) && (dDateTo.Day() < dDOB.Day()) {
		lnAge = lnAge - 1
		lnMonths = 11 // Not quite a full year yet!
	}

	return lnAge, lnMonths
}

func DecimalAgeDays(dDOB time.Time, dDateTo time.Time) float64 {
	var dtDOB float64
	var dtDateTo float64

	dtDOB = Julian(dDOB)
	dtDateTo = Julian(dDateTo)

	return (dtDateTo - dtDOB) / 365.2545 // 365.2545
}

// I never need the DecimalAge - I always need a Age, Months
/*
func DecimalAge(dDOB time.Time, dDateTo time.Time) float64 {
	// look to see if I can come up with a quick way of doing it.
	var result float64
	var nAge float64
	//int wLastBDayYear;
	//DateTime dLastBirthday;
	//int iDaysDiff;
	//int wYear = 0;
	//int wMonth = 0;
	//int wDay = 0;

	// Lets look at this at a more efficent way of doing it.
	// we can just get decimal yrs isnt that close enough for what we want?
	// rather than going threw all this crap code - and leap year BS.
	// calculate the int Age

	//    WeeksBetween(dFromDate time.Time, dToDate time.Time) float64 {

	// thinking about this one - we want an int for the years
	// then a day percentage

	iIntAge := YearsBetween(dDOB, dDateTo)
	nAge = float64(iIntAge)
	// calculate the date of the Birthday
	wYearFrom, wMonthFrom, wDayFrom := dDOB.Date()

	wLastBDayYear := (wYearFrom + iIntAge)
	// if the data is Feb 29th and not a leap year, adjust it back to the 28th
	if (wMonthFrom == 2) && (wDayFrom == 29) && (!IsLeapYear(wLastBDayYear)) {
		wDayFrom = 28
	}
	dLastBirthday := time.Date(wLastBDayYear, wMonthFrom, wDayFrom, 0, 0, 0, 0, time.UTC)
	// calculate the days difference between the last Birthday and dDateTo
	iDaysDiff := DaysBetween(dLastBirthday, dDateTo)
	// add the days as a fraction to the Age
	if iDaysDiff > 0 {
		// work out whether dtDateTo is in a leap year
		wYear, wMonth, wDay := dDateTo.Date()
		if IsLeapYear(wYear) || IsLeapYear(wYear-1) {
			// if there was a LEAP DAY between dtLastBirthday and dDateTo
			result = (nAge + iDaysDiff/366)
		} else {
			result = (nAge + iDaysDiff/365)
		}
	} else {
		result = nAge
	}
	return result
}
*/
//calculate the Julian  date, provided it's within 209 years of Jan 2, 2006.
// Drawn from
// http://grokbase.com/t/gg/golang-nuts/141a9p1dfd/go-nuts-how-to-calculate-time-passed-since-a-date-in-years-months-and-days-and-combinations
// http://play.golang.org/p/ocYFWY7kpo
func Julian(t time.Time) float64 {
	// Julian date, in seconds, of the "Format" standard time.
	// (See http://www.onlineconversion.com/julian_date.htm)
	const julian = 2453738.4195
	// Easiest way to get the time.Time of the Unix time.
	// (See comments for the UnixDate in package Time.)
	unix := time.Unix(1136239445, 0)
	const oneDay = float64(86400. * time.Second)
	return julian + float64(t.Sub(unix))/oneDay
}

func StrtoMonth(value string) int {
	if strings.Contains(value, "Jan") {
		return 1
	}
	if strings.Contains(value, "Feb") {
		return 2
	}
	if strings.Contains(value, "Mar") {
		return 3
	}
	if strings.Contains(value, "Apr") {
		return 4
	}
	if strings.Contains(value, "May") {
		return 5
	}
	if strings.Contains(value, "Jun") {
		return 6
	}
	if strings.Contains(value, "Jul") {
		return 7
	}
	if strings.Contains(value, "Aug") {
		return 8
	}
	if strings.Contains(value, "Sep") {
		return 9
	}
	if strings.Contains(value, "Oct") {
		return 10
	}
	if strings.Contains(value, "Nov") {
		return 11
	}
	if strings.Contains(value, "Dec") {
		return 12
	}

	return 0 // unknown
}

// Endof the Day as in time
// 0.5 = Noon
func EndofDay() time.Time {
	//result = 0.5 + new DateTime(0,0,0,11, 59 ,59 ,0);
	return time.Date(0, 0, 0, 23, 59, 59, 0, time.UTC)
}

func EOD(value time.Time) time.Time {
	return time.Date(value.Year(), value.Month(), value.Day(), 23, 59, 59, 0, time.UTC)
}


/*
aim is reduce this function list to make sure I have everything I need covered.
I am going to run into how to use a number of these things so find an example
to speed things up. - even do an example if need be.

2014-10 : Ok most things have been covered.
Quite few deprecated - I really only wanted the items that I absolutely needed to move forward.
TODO: fiscal func's need to be moved to a specific package.

// assumes value is time
    class function  Day(dDate: TdateTime): integer;
// covered by  value.Day()
    class function  Month(dDate: TdateTime): integer;
// value.Month()  - returns a Month type which has string() attached to it.

    class function  Year(dDate: TdateTime): integer;
// Value.Year

    class function  Hour(dDate: Tdatetime): integer;
// Value.Hour()

    class function  Minute(dDate : Tdatetime) : integer;
// Value.Minute()

    class function  MinSecMSec : string;
// Value.Nanosecond()
// Value.Second()

    class function  OntheHour(dDate : Tdatetime) : TDatetime;
// can use the round function
// value.Round(d Duration) Time

    class function  IsLeapYear(iYear: integer): boolean;
// Done

    class function  DaysPerMonth(dDate: TDateTime): integer;
// Done

    class function  AddMonths(dDate: TDateTime; iMonths: integer): TDateTime;
// AddDate
    class function  AddPeriod(adtDate : TDatetime; aiYears, aiMonths, aiDays : integer) : TDatetime;
// AddDate
    class function  DaysBetween(dFromDate, dToDate: TDateTime): integer;
// Done - DaysBetween - what out on the values may need further testing here.

    class function  YearsBetween(d1, d2: TDateTime): integer;
// Not Really used anywhere and I question what it actually does is correct

    class function  DecimalYearsBetween(dFromDate, dToDate: TDateTime): double;
// Done DecimalYearsBetween() - converts to julian to help out.

    class function  WeeksBetween( StartingDate,EndingDate : TDatetime ) : double;
// Done

    class function  TimeBetweensec(adtStartTime, adtEndTime : TDatetime ) : integer;

    class function  GetDayNo(dDate : TDatetime) : integer;
// dDate.Day()  - will do the job
// or YearDay()

    class function  GetWeeksNo( dDate : TDatetime ) : integer;
// Done - GetWeekCalenderWeekNo()

    class function  EncodeDMW( aidow , aiweek, aimonth, aiyear : word ) : TDatetime;
// I dont see a need for it any more.

    class function  JusttheDate( adtDate: TDatetime) : TDatetime;
// Done TheDate()
    class function  JusttheTime( adtTime: TDatetime) : TDatetime;
// Done TheTime()

    class function  AddTime( adtTime : TDatetime; adtPeriod : TDatetime ) : TDatetime;
// Not used.

    class function  GetYearValue( adtDate: TDatetime) : integer;

// I need to finish off the below method
//    class function  DateTimeBetween( aptPeriod : ptPeriod; d1, d2 : TDatetime; aiSetPeriod : integer = 0 ) : integer;

    class function  Age(dDOB, dDateTo: TDateTime): integer;
// Age

    class function  DecimalAge(dDOB, dDateTo: TDateTime): double;
// AgenMonths - wrote something that we actually need.

    class function  AussieStringToDateTime(const sDate: string): TDateTime;
// Not needed
    class function  JulianDate(d: TDateTime): integer;
// Done.

    class function  GetMonthStartDate(d: TDateTime): TDateTime;
// Not being used.

    class function  GetMonthEndDate(d: TDateTime):TDateTime;
    class function  GetLastMonthDate(d: TDateTime): TDateTime;

    class function  GetDateMinusYears(d: TDateTime; iYears: integer): TDateTime;

    class function  StrDayOfWeek(d: TDateTime): string;
// Date dim pop - see WeekDay() -

    class function GetDayOfTheWeek(aDay: integer): string;
    class function  StrMonthOfYear(d: TDateTime): string;
// See Month
    class function  StrtoMonth(str : string) : integer;
// Done StrtoMonth() int - returning int at the moment.


//  class function  EndofWeek(d: TDatetime; epoch : integer = 7);
    class function  WeekDay(d: TDateTime) : integer; // Return 1 True;

    class function  EOD(d:TDatetime) : TDatetime;
// Done
    class function  EndofDay : TDatetime;
// Done

    class function  NoofWeeks( StartingDate : TDatetime ) : integer;
// Deprecated
//    class function  MonthofWeekNo( Starting : TDatetime; WeekNo : integer ) : integer;


    class function  GetMonthStr( aiMonth : integer ) : string;
    class function  GetMonthShortStr( aiMonth : integer ) : string;
    class procedure GetMonthsFrom( aiMonthFrom : integer; aStrings : TStrings );
    class function  GetMonthStrFrom( aiMonthFrom : integer; aiMonthNo : integer ) : string;



    class procedure ValidWeekPeriod(iday : integer; d1, d2 : TDatetime);
// Not used - Deprecated
    class procedure ValidWeekPeriodDays(iday,idays : integer; d1, d2 : TDatetime);
// Not used - Deprecated
    class procedure ValidMonthPeriod(d1, d2 : TDatetime);
// Not used - Deprecated
    class procedure ValidMonthlyPeriod(d1, d2 : TDatetime; iStartDay : integer);
// Not used - Deprecated

    // Calculate date for Start Date of this Financial Year
    class function  GetThisFinYear(Country : TCountries; d: TDateTime): TDateTime;
// I dont want this implemented here - I want a special structure to fix host all these funcs that go with it.
    // Calculate date for Start Date of last Financial Year
    class Function  GetLastFinYear(Country : TCountries; d: TDateTime): TDateTime;

    class function  WhatFiscalQuarter(FinyearMonth : integer; d:TDatetime): integer;
    class function  WhatCalenderQuarter(d:TDatetime): integer;
// fiscal package - to be created.

    //Retrieve the DateOrder the Format Resides
    class function  GetDateOrder(const DateFormat: string): TDateOrder;
//Deprecated Not used anymore

    // Returns a Datetime via String
    class function  dtStrtoDate(sDateStr : String) : TDatetime;
    class function  dtStrtoDateFormat(sFormat : string; sDateStr : String) : TDatetime;
// These two are being used but I dont believe they are relevant - comeback to them

    // Checks the Formats if the ShortDate Format is Valid ...
    class function  IsValidShortDateFormat(sDateStr : string) : boolean;

    class function  iso8601StrtoDate(sDate : string) : TDatetime;
    class function  YYYYMMDDStrtoDate(sDate : string) : TDatetime;
    class function  HHNNStrtoTime(sTime : string) : TDatetime;
    class function  HHNNtoTime(sTime : string) : TDatetime;

    // Give it a Year and it will return a valid one..
    class function  ProcessValidYear(iYear : integer) : integer;
// Deprecate- this used to be for two digit years no longer relevant.

    class function  dtQueryDate(dtDate : TDatetime) : string;

    // Adjust the ShortDate Format ....
    class procedure AdjustFormatSettings;

    //New yet to be comleted
    class function  FormattedDatestringtoDatetime( sFormat, sDate : string ) : TDatetime;

    class function  NextBusinessDay : TDateTime;

// Delphi Remapped Functions
    class procedure decodedate(Date : TDatetime; var Year, Month, Day : word);

// Periods
    class function  Periodsof( adtStart, adtEnd : TDatetime; appFrequency : TePeriods ) : TList;
    class function  IsDateinPeriod( appFrequency : TePeriods; adtDate : TDatetime; aoPeriods : TList; var aoDatePeriod : TDatePeriod ) : boolean;
    class function  StartofPeriod( adtDate : TDatetime; appFrequency : TePeriods  ) : TDatetime;
// No idea what is for and why?

// Format
    class function  FormatDT( aFormat : string; aDate : TDatetime  ) : string;

    class function  HundredstoDT( aiDatetime : word ) : TDatetime;

    class function  IsNullDate( adtDate : TDatetime ) : boolean;
    class function  GetDateAsString( adtDate : TDateTime; sEmptyDate: string = '' ) : string;
    class function  FormatYearAndMonths( adtStartDate: TDateTime; adtEndDate: TDateTime;
      sPrefixString: string = ''; sSuffixString: string = ''; sFormatString: string = '{prefix} {years}Y {months}M, {y.m} {suffix}' ) : string;
    class function  WeeksDefined(adtStart : TDatetime) : TStringlist;

    class procedure ValidateDatePeriod(aStartDate, aFinishDate: TDateTime;
      ValidationRules: TDatePeriodValidationRules; const ErrorMessages: Variant);
*/
