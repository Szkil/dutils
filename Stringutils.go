package dutils

import (
	"fmt"
	"strings"
"crypto/rand"
	"errors"
	"unicode"
)

func IIF_str(value bool, str1 string, str2 string) string {
	if value {
		return str1
	} else {
		return str2
	}
}

func SqlFormatCheck(value string) string {
	if strings.Contains(value, "%") {
		return strings.Replace(value, "%", "%%", 1)
	} else {
		return value
	}
}

func GetIntstring(aoInts []int64) string {
	result := "";

	if len(aoInts) == 0 {
		return result
	}
	if len(aoInts) == 1 {
		result = fmt.Sprintf(" %v ", aoInts[0])
		return result
	}

	lsSelect := ""
	for liItem, _ := range aoInts {
			lsSelect = lsSelect + fmt.Sprintf("%v ",liItem) + ", "
		}
	lsSelect = strings.TrimSuffix(lsSelect, ", ")

	return result
}

func Pseudo_uuid() (uuid string) {

	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	uuid = fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

	return
}

//  b := substring(guid, 4, 8)
func Substring(s string, start int, end int) string {
	start_str_idx := 0
	i := 0
	for j := range s {
		if i == start {
			start_str_idx = j
		}
		if i == end {
			return s[start_str_idx:j]
		}
		i++
	}
	return s[start_str_idx:]
}

// CopyStrTo O to n
func CopyStrTo(a string, n int) string {
	if len(a) == 0 {
		return ""
	}
	return a[0:n]
}

// CopyStrFromto x to z
func CopyStrFromto(a string, x int, z int) string {
	if len(a) == 0 {
		return ""
	}
	return a[x:z]
}

// BooltoInt - T|True|Y|Yes = 1 else 0
func BooltoInt(value string) int {
	if value == "T" || value == "True" || value == "Y" || value == "Yes"  {
		return 1
	} else {
		return 0
	}
}

func BooltoStr(value bool) string {
	if value {
		return "T"
	} else {
		return "f"
	}
}

func StrToBool(value string) bool {
	if value == "T" || value == "True" || value == "Y" || value == "Yes"  {
		return true
	} else {
		return false
	}
}

func BooltoStrSystem(value bool) string {
	if value {
		return "T"
	} else {
		return "F"
	}
}


var code = []byte("01230127022455012623017202")

// sundex - obtained - http://rosettacode.org/wiki/Soundex#Go
func Soundex(s string) (string, error) {
	var sx [4]byte
	var sxi int
	var cx, lastCode byte
	for i, c := range s {
		switch {
		case !unicode.IsLetter(c):
			if c < ' ' || c == 127 {
				return "", errors.New("ASCII control characters disallowed")
			}
			if i == 0 {
				return "", errors.New("initial character must be a letter")
			}
			lastCode = '0'
			continue
		case c >= 'A' && c <= 'Z':
			cx = byte(c - 'A')
		case c >= 'a' && c <= 'z':
			cx = byte(c - 'a')
		default:
			return "", errors.New("non-ASCII letters unsupported")
		}
		// cx is valid letter index at this point
		if i == 0 {
			sx[0] = cx + 'A'
			sxi = 1
			continue
		}
		switch x := code[cx]; x {
		case '7', lastCode:
		case '0':
			lastCode = '0'
		default:
			sx[sxi] = x
			if sxi == 3 {
				return string(sx[:]), nil
			}
			sxi++
			lastCode = x
		}
	}
	if sxi == 0 {
		return "", errors.New("no letters present")
	}
	for ; sxi < 4; sxi++ {
		sx[sxi] = '0'
	}
	return string(sx[:]), nil
}