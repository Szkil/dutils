package dutils

import (
	"fmt"
	"testing"
	"time"
)

// Day return
func Test_Day(t *testing.T) {
	// todo: This is not doing anything - correct this
	ldtDay := time.Date(2001, 1, 16, 0, 0, 0, 0, time.UTC)

	if ldtDay.Day() != 16 {
		t.Error("Day did not return expected value of 16 :- ", ldtDay.Day())
	}
}

func Test_IsaLeapYear(t *testing.T) {
	if IsLeapYear(2000) == false {
		t.Error("error 2000 is leap year")
	}

	if IsLeapYear(2004) == false {
		t.Error("error 2004 is leap year")
	}

	if IsLeapYear(2008) == false {
		t.Error("error 2008 is leap year")
	}

}

func Test_IsaNotLeapYear(t *testing.T) {
	if IsLeapYear(1999) == true {
		t.Error("error 1999 is leap year")
	}

	if IsLeapYear(2005) == true {
		t.Error("error 2004 is leap year")
	}

	if IsLeapYear(2009) == true {
		t.Error("error 2009 is leap year")
	}

}

func Test_DaysInMonth_is(t *testing.T) {

	if DaysInMonth(2, 2000) != 29 {
		t.Error("Error in DaysInMonth")
	}

	ldtDay := time.Date(2001, 1, 1, 1, 1, 1, 1, time.UTC)
	if DaysInMonth(ldtDay.Month(), ldtDay.Year()) != 31 {
		t.Error("Error in DaysInMonth(Jan) ")
	}

}

func Test_DaysBetween(t *testing.T) {
	//dFromDate time.Time, dToDate time.Time) int {
	ldtDay01 := time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)
	ldtDay02 := time.Date(2002, 1, 1, 0, 0, 0, 0, time.UTC)

	result := DaysBetween(ldtDay01, ldtDay02)
	if result != 365 {
		t.Error("DaysBetween() of a year returned ", result)
	}
	// test the reversal should get the same behavior
	result = DaysBetween(ldtDay02, ldtDay01)
	if result != 365 {
		t.Error("DaysBetween() of a year returned ", result)
	}

}

// years test if the var is modified after it comes back.
// DecimalYearsBetween(dFromDate time.Time, dToDate time.Time) float64 {
func Test_DecimalYearsBetween(t *testing.T) {
	//(dFromDate time.Time, dToDate time.Time) float64
	ldtDay01 := time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)
	ldtDay02 := time.Date(2002, 1, 1, 0, 0, 0, 0, time.UTC)

	result := DecimalYearsBetween(ldtDay01, ldtDay02)
	if result != 1 {
		t.Error("DecimalYearsBetween() of a year returned ", result)
	}

	ldtDay02 = time.Date(2002, 7, 2, 12, 0, 0, 0, time.UTC)
	result = DecimalYearsBetween(ldtDay01, ldtDay02)
	if result != 1.5 {
		t.Error("DecimalYearsBetween() of a year returned ", result)
	}

}

func Test_WeeksBetween(t *testing.T) {
	ldtDay01 := time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)
	ldtDay02 := time.Date(2001, 1, 1, 0, 0, 0, 0, time.UTC)

	result := WeeksBetween(ldtDay01, ldtDay02)
	if result != 0 {
		t.Error("Incorrect WeeksYearsBetween() of -  ", result)
	}

	ldtDay02 = time.Date(2002, 7, 1, 0, 0, 0, 0, time.UTC)
	result = WeeksBetween(ldtDay01, ldtDay02)
	if result != 78 {
		t.Error("Incorrect WeeksYearsBetween() of -  ", result)
	}

	// Swap them around so we have a positive
	result = WeeksBetween(ldtDay02, ldtDay01)
	if result != 78 {
		t.Error("Incorrect WeeksYearsBetween() of -  ", result)
	}

	// Huge No.
	ldtDay01 = time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC)
	ldtDay02 = time.Date(2100, 1, 1, 0, 0, 0, 0, time.UTC)
	result = WeeksBetween(ldtDay01, ldtDay02)
	if result < 10000 { // should be 10400
		t.Error("Incorrect WeeksYearsBetween() of -  ", result)
	}

}

func Test_TheDate(t *testing.T) {

	ldtDay01 := time.Date(2001, 1, 1, 3, 15, 3, 4, time.UTC)

	ldtDay01 = TheDate(ldtDay01)
	liHour, liMin, liSec := ldtDay01.Clock()
	if liHour > 0 || liMin > 0 || liSec > 0 {
		t.Error("TheDate has time components that are not zero'd - ", liHour, liMin, liSec)
	}

	fmt.Println("TheDate value - ", liHour, liMin, liSec)
	//	if TheDate(ldtDay01) hour and min sec > 0/1 then we have a problme.

	//func TheDate(dDate time.Time) time.Time {
	//	liYear, liMonth, liDay := dDate.Date()
	//	return time.Date(liYear, liMonth, liDay, 0, 0, 0, 0, time.UTC)
}

func Test_TheTime(t *testing.T) {
	// This needs a review as what do I really need the time for and what is the date component supposed
	// to be really - I think its situational.

	ldtDay01 := time.Date(2001, 1, 1, 3, 15, 3, 4, time.UTC)

	ldtDay01 = TheTime(ldtDay01)
	liHour, liMin, liSec := ldtDay01.Clock()

	if liHour != 3 || liMin != 15 || liSec != 3 {
		t.Error("TheDate has time components that are zero'd - ", liHour, liMin, liSec)
	}

	// At the moment the return is the current day - I may change this depending
	// on what I need to use it for.  - watch out!
	liYear, liMonth, liDay := ldtDay01.Date()
	if liYear != ldtDay01.Year() {
		t.Error("Year changed ", liYear)
	}
	if liMonth != ldtDay01.Month() {
		t.Error("Month changed ", liMonth)
	}
	if liDay != ldtDay01.Day() {
		t.Error("Day changed ", liDay)
	}

}

func Test_Age(t *testing.T) {
	// I thinking there maybe an issue here.
	ldtDay01 := time.Date(2000, 2, 28, 0, 0, 0, 0, time.UTC)
	ldtNow := time.Date(2010, 2, 28, 0, 0, 0, 0, time.UTC)

	lnAge := Age(ldtDay01, ldtNow)
	if lnAge != 10 {
		t.Error("Age is expecting 10 but gather - ", lnAge)
	}

	ldtDay01 = time.Date(2000, 12, 28, 0, 0, 0, 0, time.UTC)
	ldtNow = time.Date(2010, 2, 28, 0, 0, 0, 0, time.UTC)
	lnAge = Age(ldtDay01, ldtNow)
	if lnAge != 9 {
		t.Error("Age is expecting 10 but gather - ", lnAge)
	}

}

func Test_AgenMonths(t *testing.T) {
	//
	// I thinking there maybe an issue here. with ldtnow month being before the birthdate
	// do some tests to make sure its all covered.

	ldtDay01 := time.Date(2000, 2, 28, 0, 0, 0, 0, time.UTC)
	ldtNow := time.Date(2010, 2, 28, 0, 0, 0, 0, time.UTC)

	lnAge, lnMonths := AgenMonths(ldtDay01, ldtNow)
	if lnAge != 10 || lnMonths != 0 {
		t.Error("Age is expecting 10 but gathered :- ", lnAge, lnMonths)
	}

	ldtDay01 = time.Date(2000, 12, 28, 0, 0, 0, 0, time.UTC)
	ldtNow = time.Date(2010, 2, 28, 0, 0, 0, 0, time.UTC)
	lnAge, lnMonths = AgenMonths(ldtDay01, ldtNow)
	if lnAge != 9 || lnMonths != 2 {
		t.Error("Age is expecting 9, 2mths but gathered - ", lnAge, lnMonths)
	}

	ldtDay01 = time.Date(2000, 12, 28, 0, 0, 0, 0, time.UTC)
	ldtNow = time.Date(2010, 11, 28, 0, 0, 0, 0, time.UTC)
	lnAge, lnMonths = AgenMonths(ldtDay01, ldtNow)
	if lnAge != 9 || lnMonths != 11 {
		t.Error("Age is expecting 9, 11mths but gathered - ", lnAge, lnMonths)
	}

	ldtDay01 = time.Date(2000, 2, 28, 0, 0, 0, 0, time.UTC)
	ldtNow = time.Date(2011, 4, 29, 0, 0, 0, 0, time.UTC)
	lnAge, lnMonths = AgenMonths(ldtDay01, ldtNow)
	if lnAge != 11 || lnMonths != 2 {
		t.Error("Age is expecting 11, 2mths but gathered - ", lnAge, lnMonths)
	}

	ldtDay01 = time.Date(2000, 2, 28, 0, 0, 0, 0, time.UTC)
	ldtNow = time.Date(2011, 4, 27, 0, 0, 0, 0, time.UTC)
	lnAge, lnMonths = AgenMonths(ldtDay01, ldtNow)
	if lnAge != 11 || lnMonths != 1 {
		t.Error("Age is expecting 11, 1mths but gathered - ", lnAge, lnMonths)
	}

}

func Test_StrtoMonth(t *testing.T) {
	// Watch out if we need to handle case.
	if StrtoMonth("Jan") != 1 {
		t.Error("Jan did not return correctly")
	}
}

/* on hold I need a meaningful tests
func Test_DecimalAges(t *testing.T) {
	// Test what happens if Birthday is a Leap year?  1978/02/29 - check
	// Test on a normal day - 1970/12/16
	// Look I am just going with a normal sub

	ldtDay01 := time.Date(2000, 2, 29, 0, 0, 0, 0, time.UTC)
	ldtDay02 := time.Date(2001, 12, 26, 0, 0, 0, 0, time.UTC)

	fmt.Println("DecimalAges value - ", DecimalAges(ldtDay01, time.Now()))
	fmt.Println("DecimalAges value - ", DecimalAges(ldtDay01, time.Date(2004, 2, 29, 0, 0, 0, 0, time.UTC)))
	fmt.Println("DecimalAges value - ", DecimalAges(ldtDay01, time.Date(2001, 3, 1, 0, 0, 0, 0, time.UTC)))
	fmt.Println("DecimalAges value - ", DecimalAges(ldtDay01, time.Date(2001, 2, 28, 0, 0, 0, 0, time.UTC)))

	fmt.Println("DecimalAges value - ", DecimalAges(ldtDay02, time.Now()))

}
*/
